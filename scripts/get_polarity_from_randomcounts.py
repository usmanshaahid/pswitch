import numpy as np
import sys



def main(args):
	REP_COUNTS = args[1]
	DEM_COUNTS = args[2]
	POL_FILE = args[3]

	rep_mat = np.load(REP_COUNTS)
	dem_mat = np.load(DEM_COUNTS)

	rep_sums = np.sum(rep_mat, axis=0)
	dem_sums = np.sum(dem_mat, axis=0)

	rep_mat = rep_mat / rep_sums
	dem_mat = dem_mat / dem_sums

	rep_mat = np.delete(rep_mat, 6, 1)
	dem_mat = np.delete(dem_mat, 6, 1)

	rep_mat += 0.00000000001
	dem_mat += 0.00000000001

	sum_mat = rep_mat + dem_mat

	dem_percent = np.divide(dem_mat, sum_mat)
	rep_percent = np.divide(rep_mat, sum_mat)

	polarity = dem_mat - rep_mat
	np.save(POL_FILE, polarity)
	


if __name__ == '__main__':
	exit(main(sys.argv))