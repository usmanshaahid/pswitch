import json
import sys
from multiprocessing import Pool
from datetime import datetime
import random




def get_tuple(line):
	d = json.loads(line)
	if 'retweeted_msg_id' in d:
		retweeted_user = d['retweeted_user']
		author = d['author']
		datetime = d['datetime']
		msg_id = d['retweeted_msg_id']
		return "{0}\t{1}\t{2}\t{3}\n".format(author, retweeted_user, msg_id, datetime)
	return None



def main(args):
	DATA_FILE = args[1]
	OUT_FILE = args[2]
	NUM_PROC = int(args[3])

	CHUNK_SIZE = 100000

	p = Pool(NUM_PROC)

	size = 0
	counter = 0
	with open(DATA_FILE, 'r') as fin, open(OUT_FILE, 'w') as fout:
		lines = []
		for line in fin:
			lines.append(line)
			if len(lines) == CHUNK_SIZE:
				filtered_lines = p.map(get_tuple, lines)
				filtered_lines = [l for l in filtered_lines if l is not None]
				random_tweet = random.choice(filtered_lines) if len(filtered_lines) > 0 else None
				lines = []
				counter += 1
				size += len(filtered_lines)

				fout.write(''.join(filtered_lines))
				print("Processed: {0}".format(CHUNK_SIZE * counter))
				print("Data Size: {0}".format(size))
				print("Random Tweet: {0}".format(random_tweet))
		else:
			filtered_lines = p.map(get_tuple, lines)
			filtered_lines = [l for l in filtered_lines if l is not None]
			random_tweet = random.choice(filtered_lines) if len(filtered_lines) > 0 else None
			lines = []
			counter += 1
			size += len(filtered_lines)

			fout.write(''.join(filtered_lines))
			print("Processed: {0}".format(CHUNK_SIZE * counter))
			print("Data Size: {0}".format(size))
			print("Random Tweet: {0}".format(random_tweet))



if __name__ == '__main__':
	exit(main(sys.argv))