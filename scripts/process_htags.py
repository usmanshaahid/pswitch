import sys
import json
import re


def load_tuples(filename):
	with open(filename) as fin:
		text = fin.read()
		return json.loads(text)


def dump(var, filename):
	with open(filename, 'w') as fout:
		fout.write(json.dumps(var))


def merge_tups(tups):
	d = {}
	for tup in tups:
		tag, freq = tup
		tag = re.sub("[^A-Za-z0-9]+", "", tag)
	
		key = tag.lower()
		if not key in d:
			d[key] = []
		d[key].append((tag, tup[1]))

	new_dict = {}
	for ltag in d:
		tags = d[ltag]
		total_freq = sum([t[1] for t in tags])
		tag = max(tags, key=lambda x: x[1])[0]
		new_dict[tag] = total_freq
	return new_dict


def merge_dicts(dem_dict, rep_dict):
	keys = set(list(dem_dict.keys()) + list(rep_dict.keys()))

	tups = dict()
	for key in keys:
		real_key = key.lower()

		dem_count = dem_dict[key] if key in dem_dict else 0
		rep_count = rep_dict[key] if key in rep_dict else 0

		if real_key in tups:
			tup = tups[real_key]
			old_count = tup[1] + tup[2]
			new_count = dem_count + rep_count

			hashtag = tup[0]
			if old_count > new_count:
				hashtag = key
			tups[real_key] = (hashtag, max(tup[1], dem_count), max(tup[2], rep_count))
		else:
			tups[real_key] = (key, dem_count, rep_count)

	return tups.values()


def compute_polarity(tups):
	pols = []
	for tup in tups:
		hashtag, dem, rep = tup
		if dem != rep:
			k = 0.9 / (dem - rep)
			
			if k < 0:
				pol = -1 * (k + 1)
			elif k > 0:
				pol = 1 - k
			pols.append((hashtag, pol))
	return pols
	

def main(args):
	dem_file = args[1]
	rep_file = args[2]


	dem_tups = load_tuples(dem_file)
	rep_tups = load_tuples(rep_file)

	dem_dict = merge_tups(dem_tups)
	rep_dict = merge_tups(rep_tups)


	tups = merge_dicts(dem_dict, rep_dict)
	pol_tups = compute_polarity(tups)
	pol_tups = sorted(pol_tups, key=lambda x: x[1])
	dump(pol_tups, 'output.txt')



if __name__ == '__main__':
	exit(main(sys.argv))