import sys
from walker import Walker


def load_list(filepath):
    with open(filepath, 'r') as fin:
    	text = fin.read()
    	return list(filter(len, text.split('\n')))


def translate_seeds(seeds, ulist):
	new_seeds = []
	for s in seeds:
		try:
			seed_idx = ulist.index(s.lower())
			new_seeds.append(seed_idx)
		except:
			pass
	return new_seeds


def main(args):
	SEEDS_FILEPATH = args[1]
	ULIST_FILEPATH = args[2]
	GRAPH_FILEPATH = args[3]
	RESTART_PROB = float(args[4])


	# dem_seeds = load_list(DEM_SEEDS_FILEPATH)
	seeds = load_list(SEEDS_FILEPATH)
	ulist = load_list(ULIST_FILEPATH)
	seeds = translate_seeds(seeds, ulist)

	print(len(seeds))

	wk = Walker(GRAPH_FILEPATH, [], [])
	wk.run_exp(seeds, RESTART_PROB, 0.1, [])


if __name__ == '__main__':
	exit(main(sys.argv))