import sys
import json
import pickle as pkl
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from datetime import datetime
import progressbar



def parse_timestamp(timestamp):
	return datetime.strptime(timestamp,'%a %b %d %H:%M:%S +0000 %Y')


def get_topic(topics):
	if 'TRUMP' in topics and 'HILLARY' in topics:
		return None
	if 'TRUMP' in topics:
		return 'T'
	if 'HILLARY' in topics:
		return 'H'
	return None


def main(args):
	DATA_FILE = args[1]
	RT_DICT = args[2]
	TIM_FILE = args[3]

	rt_dict = None
	with open(RT_DICT, 'rb') as fin:
		rt_dict = pkl.load(fin)

	user_dict = {}

	analyser = SentimentIntensityAnalyzer()
	bar = progressbar.ProgressBar(max_value=24613385)

	with open(DATA_FILE) as fin:
		for line in bar(fin):
			d = json.loads(line)
			if 'retweeted_msg_id' in d:
				rt_id = d['retweeted_msg_id'].split('_')[-1]
				sentiment = rt_dict[rt_id]
			else:
				sentiment = analyser.polarity_scores(d['text'])
			
			timestamp = parse_timestamp(d['datetime']).strftime("%Y-%m-%d %H:%M:%S")
			
			author = d['author'].lower()
			sentiment = sentiment['compound']

			if not author in user_dict:
				user_dict[author] = []

			topics = set(d['topics'][0][1])
			topic = get_topic(topics)
			
			if topic is not None:	
				user_dict[author].append((timestamp, sentiment, topic))

	with open(TIM_FILE, 'wb') as fout:
		pkl.dump(user_dict, fout)


if __name__ == '__main__':
	exit(main(sys.argv))