import sys
from neo4j.v1 import GraphDatabase
from datetime import datetime, timedelta
import random
import progressbar
import pickle as pkl
import numpy as np


def parse_date(dstr):
	return datetime.strptime(dstr, '%Y-%m-%d')


def get_day(date):
	START_DATE = datetime.strptime('1Jan2016', '%d%b%Y')
	return (date - START_DATE).days


def load_list(filepath):
	with open(filepath) as fin:
		lines = fin.read().split('\n')
		return list(filter(len, lines))


def update_user(session, user, seed, start_day, end_day, restart_prob):
	if random.random() < restart_prob:
		return seed
	
	query = "MATCH (u:User)-[r]-(t:Tweet) WHERE u.name = {name} AND r.day >= {start} AND r.day < {end} RETURN t.name"
	results = session.run(query, name=user, start=start_day, end=end_day)
	tweets = [r['t.name'] for r in results]
	if len(tweets) == 0:
		return seed
	tweet = random.choice(tweets)
	query = "MATCH (t:Tweet)-[r]-(u:User) WHERE t.name = {name} AND r.day >= {start} AND r.day < {end} RETURN u.name"
	results = session.run(query, name=tweet, start=start_day, end=end_day)
	users = [r['u.name'] for r in results]
	if len(users) == 0:
		return seed
	return random.choice(users)



def run_random_walker(seeds, start_day, end_day, uinv_index, topic):
	NUM_ITER = 10000
	RESTART_PROB = 0.75

	counts = np.zeros(len(uinv_index.keys()))
	driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
	with driver.session() as session:
		users = seeds[:]
		bar = progressbar.ProgressBar()
		for i in bar(range(NUM_ITER)):
			for i in range(len(users)):
				idx = uinv_index[users[i]]
				counts[idx] += 1
				users[i] = update_user(session, users[i], seeds[i], start_day, end_day, RESTART_PROB)
	print(sum(counts != 0))
	return counts


def load_obj(obj_filepath):
	with open(obj_filepath, 'rb') as fin:
		return pkl.load(fin)


def main(args):
	REP_SEEDS = args[1]
	DEM_SEEDS = args[2]
	USER_DICT = args[3]

	START_DATE = parse_date(args[4])
	END_DATE = parse_date(args[5])
	delta = int(args[6])

	REP_MAT_FILEPATH = args[7]
	DEM_MAT_FILEPATH = args[8]

	dem_seeds = load_list(DEM_SEEDS)
	rep_seeds = load_list(REP_SEEDS)
	uinv_index = load_obj(USER_DICT)

	dem_mat = None
	rep_mat = None

	curr_start = START_DATE
	while curr_start < END_DATE:
		curr_end = curr_start + timedelta(days=delta)
		if curr_end > END_DATE:
			curr_end = END_DATE

		print("====================== Running for interval {0} to {1} ======================".format(curr_start, curr_end))

		start_day = get_day(curr_start)
		end_day = get_day (curr_end)

		print("Running random walker for democrats...")
		dem_counts = run_random_walker(dem_seeds, start_day, end_day, uinv_index, 'D')
		dem_counts = dem_counts.reshape(dem_counts.shape[0], 1)
		if dem_mat is None:
			dem_mat = dem_counts
		else:
			dem_mat = np.hstack((dem_mat, dem_counts))

		print("Running random walker for republicans...")
		rep_counts = run_random_walker(rep_seeds, start_day, end_day, uinv_index, 'R')

		rep_counts = rep_counts.reshape(rep_counts.shape[0], 1)
		if rep_mat is None:
			rep_mat = rep_counts
		else:
			rep_mat = np.hstack((rep_mat, rep_counts))

		curr_start += timedelta(days=delta)
		print("\n\n")

	np.save(DEM_MAT_FILEPATH, dem_mat)
	np.save(REP_MAT_FILEPATH, rep_mat)


if __name__ == '__main__':
	exit(main(sys.argv))