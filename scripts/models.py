from sqlalchemy import Column, String, Integer, Date, TIMESTAMP, Text, ForeignKey, BLOB, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.orm import relationship, backref
import datetime

Base = declarative_base()

class RetweetModel(Base):
    __tablename__ = 'retweets'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(String(15))
    retweeted_user = Column(String(15))
    msg_id = Column(BIGINT(unsigned=True))
    timestamp = Column(TIMESTAMP)