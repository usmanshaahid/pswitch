from sqlalchemy import create_engine
from models import *
import sys


def main(args):
	engine =  create_engine('mysql+mysqldb://root:letmein@localhost/pswitch')
	connection = engine.connect()
	Base.metadata.create_all(engine)



if __name__ == '__main__':
	exit(main(sys.argv))