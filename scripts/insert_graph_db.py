import sys
import json
from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client
from datetime import datetime


def get_topic(topics):
	if 'TRUMP' in topics and 'HILLARY' in topics:
		return None
	if 'TRUMP' in topics:
		return 'T'
	if 'HILLARY' in topics:
		return 'H'
	return None


def get_day(timestamp):
	START_DATE = datetime.strptime('1Jan2016', '%d%b%Y')
	date = datetime.strptime(timestamp,'%a %b %d %H:%M:%S +0000 %Y')
	return (date - START_DATE).days


def main(args):
	filepath = args[1]

	db = GraphDatabase("http://localhost:7474", username="neo4j", password="letmein")
	user_label = db.labels.create("User")
	tweet_label = db.labels.create("Tweet")

	inserted = 0
	processed = 0
	TOTAL_RECORDS = 21301032
	tx = db.transaction(for_query=True)


	seen_users = set()
	seen_messages = set()

	with open(filepath) as fin:
		for line in fin:
			d = json.loads(line)
			processed += 1
			if 'retweeted_msg_id' in d:
				retweeted_user = d['retweeted_user']
				author = d['author']
				timestamp = d['datetime']
				msg_id = d['retweeted_msg_id']
				topics = set(d['topics'][0][1])
				topic = get_topic(topics)
				text = d['text']

				author = author.lower()
				retweeted_user = retweeted_user.lower()
				msg_id = msg_id.split('_')[-1]
				day = get_day(timestamp)
				sentiment = 10

				if topic is not None:
					# if not retweeted_user in seen_users:
					tx.append("MERGE (p:User {{ name: '{0}'}})".format(retweeted_user))
						# seen_users.add(retweeted_user)
					
					# if not author in seen_users:
					tx.append("MERGE (p:User {{ name: '{0}'}})".format(author))
						# seen_users.add(author)

					# if not msg_id in seen_messages:
					tx.append("MERGE (t:Tweet {{ name: '{0}', sentiment: {1} , topic: '{2}'}})".format(msg_id, sentiment, topic))
					tx.append("MATCH (rt:User), (t:Tweet) WHERE rt.name='{0}' AND t.name='{1}' MERGE (rt)-[r:TWEETED]->(t) SET r.day={2}".format(retweeted_user, msg_id, day))
						# seen_messages.add(msg_id)

					tx.append("MATCH (t:Tweet), (u:User) WHERE t.name='{0}' AND u.name='{1}' MERGE (t)-[r:RETWEETED {{ day: {2} }}]->(u)".format(msg_id, author, day))
					
					inserted += 1
					if inserted % 500 == 0:
						tx.commit()
						del tx
						tx = db.transaction(for_query=True)
						print("{0:.2f}% complete".format(processed * 100 / TOTAL_RECORDS))

if __name__ == '__main__':
	exit(main(sys.argv))