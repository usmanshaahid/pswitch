from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import sys
import pickle as pkl
import progressbar


def load_object(filepath):
	with open(filepath, 'rb') as fin:
		return pkl.load(fin)


def main(args):
	RT_TEXT_FILEPATH = args[1]
	RT_SENT_FILEPATH = args[2]

	rt_text_dict = load_object(RT_TEXT_FILEPATH)

	analyser = SentimentIntensityAnalyzer()
	
	bar = progressbar.ProgressBar()
	
	rt_sent_dict = {}
	for key in bar(rt_text_dict):
		text = rt_text_dict[key]
		analysis = analyser.polarity_scores(text)
		rt_sent_dict[key] = analysis


	with open(RT_SENT_FILEPATH, 'wb') as fout:
		pkl.dump(rt_sent_dict	, fout)


if __name__ == '__main__':
	exit(main(sys.argv))