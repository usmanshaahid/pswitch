from neo4j.v1 import GraphDatabase
import sys
import progressbar
import pickle as pkl
import time



def load_dict(SENT_DICT):
	with open(SENT_DICT, 'rb') as fin:
		return pkl.load(fin)



def fetch_tweet_ids(db):
	query = "MATCH (t:Tweet) RETURN t.name"
	with db.session() as session:
		results = session.run(query)
		return [r['t.name'] for r in results]


def main(args):
	SENT_DICT_FILEPATH =  args[1]
	
	sent_dict = load_dict(SENT_DICT_FILEPATH)
	
	driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
	bar = progressbar.ProgressBar()
	tweet_ids = fetch_tweet_ids(driver)
	
	with driver.session() as session:
		for idx, key in enumerate(bar(tweet_ids)):
			sentiment = sent_dict[key]['compound']
			query = "MATCH(t:Tweet) WHERE t.name = {name} SET t.sentiment={sent}"
			session.run(query, name=key, sent=sentiment)
			if idx % 100 == 0:
				session.sync()

if __name__ == '__main__':
	exit(main(sys.argv))