import sys
import pickle as pkl


def load_object(filepath):
	with open(filepath, 'rb') as fin:
		return pkl.load(fin)



def main(args):
	ID_MAP_FILEPATH = args[1]
	TWEET_SENT_FILEPATH = args[2]
	RT_SENT_FILEPATH = args[3]

	tweet_sent = load_object(TWEET_SENT_FILEPATH)
	id_map = load_object(ID_MAP_FILEPATH)

	rt_sent = {}
	for rt_id in id_map:
		t_id = id_map[rt_id]
		sent = tweet_sent[t_id]
		rt_sent[rt_id] = sent


	with open(RT_SENT_FILEPATH, 'wb') as fout:
		pkl.dump(rt_sent, fout)

if __name__ == '__main__':
	exit(main(sys.argv))