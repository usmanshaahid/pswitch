import sys
import json
from datetime import datetime
from neo4j.v1 import GraphDatabase
import progressbar



def execute_transactions(db, queries):
	with db.session() as session:
		with session.begin_transaction() as tx:
			for q in queries:
				result = tx.run(q)
			tx.commit()


def fetch_tweet_ids(db):
	query = "MATCH (t:Tweet) RETURN t.name"
	with db.session() as session:
		results = session.run(query)
		return [r['t.name'] for r in results]
	

def execute_queries(db, queries):
	with db.session() as session:
		with session.begin_transaction() as tx:
			for q in queries:
				result = tx.run(q)
			tx.commit()


def fix_date(session, tweet_id):
	query = "MATCH (t:Tweet)-[r:RETWEETED]->(u:User) WHERE t.name={name} RETURN MIN(r.day)"
	result = session.run(query, name=tweet_id)
	minim = result.single()['MIN(r.day)']
	query = "MATCH (u:User)-[r:TWEETED]->(t:Tweet) WHERE t.name={name} SET r.day={minim}"
	result = session.run(query, name=tweet_id, minim=minim)


def main(args):
	db = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
	tweet_ids = fetch_tweet_ids(db)

	TOTAL_RECORDS = len(tweet_ids)

	with db.session() as session:
		queries = []
		bar = progressbar.ProgressBar()
		for idx, tweet_id in enumerate(bar(tweet_ids)):
			fix_date(session, tweet_id)

			# q = 
			# queries.append(q)
			# if (idx + 1) % 1000 == 0 or (idx + 1) == TOTAL_RECORDS:
			# 	print(queries)
			# 	# execute_queries(db, queries)

if __name__ == '__main__':
	exit(main(sys.argv))