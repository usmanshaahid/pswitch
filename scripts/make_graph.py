import os
import sys
import numpy as np
import matplotlib.pyplot as plt




def make_cdf(arr, label, color):
    count, edges = np.histogram(arr, bins=5000)

    cumsum = np.cumsum(count)
    cumsum = cumsum / sum(count)

    x_vals = edges[1:]
    y_vals = cumsum

    y_vals = np.append(cumsum, 1)
    y_vals = np.insert(y_vals, 0, 0)
    
    x_vals = np.append(x_vals, np.max(x_vals))
    x_vals = np.insert(x_vals, 0, np.min(x_vals))

    plt.plot(x_vals, y_vals, label=label, color=color)


def load_list(filepath):
	with open(filepath) as fin:
		return list(filter(len, fin.read().split('\n')))



def main(args):
	RES_DIR = args[1]

	
	filenames = os.listdir(RES_DIR)
	dates = sorted(list(set(['-'.join(d.split('.')[0].split('-')[:-1]) for d in filenames])))
	
	sq_errs = []
	for date in dates:
		print("Processing {0}...".format(date))
		upol_filepath = os.path.join(RES_DIR, "{0}-upol.txt".format(date))
		upol_pred_filepath = os.path.join(RES_DIR, "{0}-upol_pred.txt".format(date))

		if os.path.isfile(upol_filepath) and os.path.isfile(upol_pred_filepath):
			upol = load_list(upol_filepath)
			upol_pred = load_list(upol_pred_filepath)

			for i in range(len(upol)):
				truth = upol[i]
				pred = upol_pred[i]
				if truth != 'nil' and pred != 'nil':
					truth = float(truth)
					pred = float(pred)
					sq_err = (truth - pred) * (truth - pred)
					sq_errs.append(sq_err)

	make_cdf(sq_errs, 'blah', 'red')
	plt.xlabel('Squared Error')
	plt.ylabel('CDF')
	plt.show()



main(sys.argv)