import json
import sys
from multiprocessing import Pool
from datetime import datetime
import random



def filter_lines(line):
	LAST_DATE = datetime.strptime('30Nov2016', '%d%b%Y')
	d = json.loads(line)
	date = datetime.strptime(d['datetime'],'%a %b %d %H:%M:%S +0000 %Y')
	if date < LAST_DATE:
		return line
	else:
		return None


def main(args):
	DATA_FILE = args[1]
	OUT_FILE = args[2]
	NUM_PROC = int(args[3])

	CHUNK_SIZE = 100000

	p = Pool(NUM_PROC)

	size = 0
	counter = 0
	with open(DATA_FILE, 'r') as fin, open(OUT_FILE, 'w') as fout:
		lines = []
		for line in fin:
			lines.append(line)
			if len(lines) == CHUNK_SIZE:
				filtered_lines = p.map(filter_lines, lines)
				filtered_lines = [l for l in filtered_lines if l is not None]
				random_tweet = random.choice(filtered_lines) if len(filtered_lines) > 0 else None
				lines = []
				counter += 1
				size += len(filtered_lines)

				fout.write(''.join(filtered_lines))
				print("Processed: {0}".format(CHUNK_SIZE * counter))
				print("Data Size: {0}".format(size))
				print("Random Tweet: {0}".format(random_tweet))
		else:
			filtered_lines = p.map(filter_lines, lines)
			filtered_lines = [l for l in filtered_lines if l is not None]
			random_tweet = random.choice(filtered_lines) if len(filtered_lines) > 0 else None
			lines = []
			counter += 1
			size += len(filtered_lines)

			fout.write(''.join(filtered_lines))
			print("Processed: {0}".format(CHUNK_SIZE * counter))
			print("Data Size: {0}".format(size))
			print("Random Tweet: {0}".format(random_tweet))



if __name__ == '__main__':
	exit(main(sys.argv))