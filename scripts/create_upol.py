import json
import sys
import os
from datetime import datetime, timedelta
import numpy as np


def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


def load_json_obj(filepath):
	with open(filepath, 'r') as fin:
		return json.loads(fin.read())


def load_list(filepath):
	with open(filepath) as fin:
		return list(filter(len, fin.read().split('\n')))


def write_list(filepath, l):
	with open(filepath, 'w') as fout:
		fout.write('\n'.join(l))


def compute_user_polarity(tweets):
	user_pol = {}
	for msg_id, timestamp, user_id, pol in tweets:
		if not user_id in user_pol:
			user_pol[user_id] = []
		user_pol[user_id].append(pol)

	for key in user_pol:
		if len(user_pol[key]) > 5:
			user_pol[key] = sum(user_pol[key]) / float(len(user_pol[key]))
		else:
			user_pol[key] = 'nil'

	return user_pol


def main(args):
	PD_FILE = args[1]
	RES_DIR = args[2]

	pol_dd = load_json_obj(PD_FILE)
	filenames = os.listdir(RES_DIR)
	dates = sorted(list(set(['-'.join(d.split('.')[0].split('-')[:-1]) for d in filenames])))

	end_dates = [datetime.strptime(d, '%Y-%m-%d') for d in dates]
	
	for end in end_dates:
		start = end - timedelta(days=15)

		tweets = []
		for date in daterange(start, end):
			date = date.strftime('%Y-%m-%d')
			if date in pol_dd:
				tweets += pol_dd[date]

		pol_dict = compute_user_polarity(tweets)
		ul_filename = "{0}-ulist.txt".format(end.strftime("%Y-%m-%d"))
		ulist = load_list(os.path.join(RES_DIR, ul_filename))

		upol = []
		for u in ulist:
			if u in pol_dict:
				upol.append(str(pol_dict[u]))
			else:
				upol.append('nil')

		up_filename = "{0}-upol.txt".format(end.strftime("%Y-%m-%d"))
		write_list(os.path.join(RES_DIR, up_filename), upol)


		new_upol = []
		for u in upol:
			if u == 'nil':
				new_upol.append(100)
			else:
				new_upol.append(float(u))

		upol = np.array(new_upol)
		ulist = np.array(ulist)

		valid_idx = (upol != 100)

		print(np.min(upol[valid_idx]))
		print(np.max(upol[valid_idx]))
		print(np.mean(upol[valid_idx]))
		print(np.std(upol[valid_idx]))


		dem_idx = (upol != 100) & (upol >= 0.3)
		rep_idx = (upol != 100) & (upol <= -0.3)

		print("---- Democrates ----")
		print(upol[dem_idx])
		print(ulist[dem_idx])

		print("---- Republicans ----")
		print(upol[rep_idx])
		print(ulist[rep_idx])


		dem_seeds_filename = "{0}-dem_seeds.txt".format(end.strftime("%Y-%m-%d"))
		rep_seeds_filename = "{0}-rep_seeds.txt".format(end.strftime("%Y-%m-%d"))


		idxs = np.arange(upol.shape[0])
		
		rep_idx = [str(r) for r in idxs[rep_idx].tolist()]
		dem_idx = [str(d) for d in idxs[dem_idx].tolist()]

		write_list(os.path.join(RES_DIR, dem_seeds_filename), dem_idx)
		write_list(os.path.join(RES_DIR, rep_seeds_filename), rep_idx)

if __name__ == '__main__':
	exit(main(sys.argv))