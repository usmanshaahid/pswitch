import sys
from datetime import datetime
import pickle as pkl
import json


def to_standard_date(timestamp):
	return timestamp.strftime('%Y-%m-%d')



def main(args):
	POL_TUP_FILE = args[1]
	OUT_DICT_FILE = args[2]


	date_dict = {}
	with open(POL_TUP_FILE) as fin:
		tups = fin.read()
		tups = json.loads(tups)

		total_tups = len(tups)
		for i in range(total_tups):
			msg_id, ts, user_id, polarity = tups[i]

			timestamp = datetime.strptime(ts, "%Y-%m-%dT%H:%M:%S.000Z")
			date = to_standard_date(timestamp)
			if not date in date_dict:
				date_dict[date] = []
			date_dict[date].append((msg_id, ts, user_id, polarity))

			if i % 10000 == 0:
				print("{0:.2f}% records processed".format(i * 100.0 / total_tups))
				
				
	
	with open(OUT_DICT_FILE, 'w') as fout:
		fout.write(json.dumps(date_dict))


if __name__ == '__main__':
	exit(main(sys.argv))