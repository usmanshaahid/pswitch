import sys
from datetime import datetime,timedelta
import pickle as pkl
import numpy as np
from scipy import sparse
import math
import random
import os


def load_dd(dd_file):
	with open(dd_file, 'rb') as fin:
		return pkl.load(fin)


def parse_date(dstr):
	return datetime.strptime(dstr, '%Y-%m-%d')


def compute_active_sim(retweets, user_idx):
	num_users = len(user_idx.keys())
	sim_mat = None

	sim = set()
	sim_dict = {}
	for author, retweeted_user, msg_id, timestamp in retweets:
		auth_idx = user_idx[author.lower()]
		rt_auth_idx = user_idx[retweeted_user.lower()]
		sim.add((rt_auth_idx, auth_idx, 1))

		if not rt_auth_idx in sim_dict:
			sim_dict[rt_auth_idx] = set()
		sim_dict[rt_auth_idx].add(auth_idx)

	sim = list(sim)
	rows = np.array([s[0] for s in sim])
	cols = np.array([s[1] for s in sim])
	data = np.array([s[2] for s in sim])
	sim_mat = sparse.csr_matrix((data, (rows, cols)), shape=(num_users, num_users))
	priors = np.sum(sim_mat, axis=0) / num_users
	priors = priors.flatten().tolist()[0]

	del sim_mat

	sum_dict = dict()
	# retweeted_users = list(sim_dict.keys())
	for retweeted_user in sim_dict:
		retweeters = sorted(list(sim_dict[retweeted_user]))
		for i in range(len(retweeters)):
			for j in range(i + 1, len(retweeters)):
				key = (retweeters[i], retweeters[j])
				if not key in sum_dict:
					sum_dict[key] = 0
				sum_dict[key] += 1

	del sim_dict
	
	flight_list= []
	for key in sum_dict:
		i, j = key
		numerator = sum_dict[key] / num_users
		den = priors[i] * priors[j]
		flight = (numerator / den) / num_users
		metric = flight
		if not math.isnan(metric) and metric > 0.001:
			flight_list.append((i, j, metric))

	del sum_dict

	rows = np.array([s[0] for s in flight_list])
	cols = np.array([s[1] for s in flight_list])
	data = np.array([s[2] for s in flight_list])


	print("Active")
	print(np.min(data))
	print(np.max(data))
	print(np.mean(data))
	print(np.std(data))

	del flight_list

	oag_mat = sparse.csr_matrix((data, (rows, cols)), shape=(num_users, num_users))
	return oag_mat


def compute_passive_sim(retweets, user_idx):
	num_users = len(user_idx.keys())
	sim_mat = None

	sim = set()
	sim_dict = {}
	for author, retweeted_user, msg_id, timestamp in retweets:
		auth_idx = user_idx[author.lower()]
		rt_auth_idx = user_idx[retweeted_user.lower()]
		sim.add((auth_idx, rt_auth_idx, 1))

		if not auth_idx in sim_dict:
			sim_dict[auth_idx] = set()
		sim_dict[auth_idx].add(rt_auth_idx)

	sim = list(sim)
	rows = np.array([s[0] for s in sim])
	cols = np.array([s[1] for s in sim])
	data = np.array([s[2] for s in sim])
	sim_mat = sparse.csr_matrix((data, (rows, cols)), shape=(num_users, num_users))
	priors = np.sum(sim_mat, axis=0) / num_users
	priors = priors.flatten().tolist()[0]

	del sim_mat

	sum_dict = dict()
	# retweeted_users = list(sim_dict.keys())
	for retweeted_user in sim_dict:
		retweeters = sorted(list(sim_dict[retweeted_user]))
		for i in range(len(retweeters)):
			for j in range(i + 1, len(retweeters)):
				key = (retweeters[i], retweeters[j])
				if not key in sum_dict:
					sum_dict[key] = 0
				sum_dict[key] += 1

	del sim_dict
	
	flight_list= []
	for key in sum_dict:
		i, j = key
		numerator = sum_dict[key] / num_users
		den = priors[i] * priors[j]
		flight = (numerator / den) / num_users
		metric = flight
		if not math.isnan(metric) and metric > 0.001:
			flight_list.append((i, j, metric))

	del sum_dict

	rows = np.array([s[0] for s in flight_list])
	cols = np.array([s[1] for s in flight_list])
	data = np.array([s[2] for s in flight_list])



	print("Passive")
	print(np.min(data))
	print(np.max(data))
	print(np.mean(data))
	print(np.std(data))

	del flight_list
	
	oag_mat = sparse.csr_matrix((data, (rows, cols)), shape=(num_users, num_users))
	return oag_mat


def get_users_index(retweets):
	users = []
	for auth, retweeted, _, _ in retweets:
		users += [auth.lower(), retweeted.lower()]
	ulist = sorted(list(set(users)))
	
	user_idx = {}
	for idx, u in enumerate(ulist):
		user_idx[u] = idx

	return user_idx, ulist



def compute_oag(retweets):
	user_idx, user_list = get_users_index(retweets)
	active_sim = compute_active_sim(retweets, user_idx)
	passive_sim = compute_passive_sim(retweets, user_idx)
	return ((0.5 * active_sim) + (0.5 * passive_sim), user_list)



def save_graph(ulist, ulistfile, mat, elistfile):
	with open(ulistfile, 'w') as fout:
		fout.write('\n'.join(ulist))

	rows, cols = mat.nonzero()
	data = mat[mat.nonzero()].flatten().tolist()[0]

	with open(elistfile, 'w') as fout:
		for i in range(len(data)):
			fout.write("{0}\t{1}\t{2}\n".format(rows[i], cols[i], data[i]))



def main(args):
	start_date = parse_date(args[1])
	end_date = parse_date(args[2])
	delta = int(args[3])
	dd_file = args[4]
	out_dir = args[5]

	dd = load_dd(dd_file)
	dstrs = dd.keys()
	dates = [parse_date(d) for d in dstrs]

	curr_start = start_date
	while curr_start < end_date:
		curr_end = curr_start + timedelta(days=delta)
		if curr_end > end_date:
			curr_end = end_date

		retweets = []
		for idx, dstr in enumerate(dstrs):
			date = dates[idx]
			if date >= curr_start and date < curr_end:
				retweets += dd[dstr]

		# del dd


		total_tweets = len(retweets)
		SAMP_SIZE = 100000
		if total_tweets > SAMP_SIZE:
			retweets = random.sample(retweets, SAMP_SIZE)

		if len(retweets) > 0:
			# del dd
			oag, user_list = compute_oag(retweets)

			if not os.path.isdir(out_dir):
				os.mkdir(out_dir)

			filename = curr_end.strftime('%Y-%m-%d')
			userlistfile = os.path.join(out_dir, "{0}-ulist.txt".format(filename))
			edgelistfile = os.path.join(out_dir, "{0}-elist.txt".format(filename))
			save_graph(user_list, userlistfile, oag, edgelistfile)

			print(curr_start, curr_end, len(retweets), (len(retweets) * 100)/ total_tweets)

			# del oag
			# dd = load_dd(dd_file)

		curr_start = curr_end

	return 0


if __name__ == '__main__':
	exit(main(sys.argv))