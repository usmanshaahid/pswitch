import sys
from datetime import datetime
import pickle as pkl


def to_standard_date(timestamp):
	return timestamp.strftime('%Y-%m-%d')



def main(args):
	RT_TUP_FILE = args[1]
	OUT_DICT_FILE = args[2]


	date_dict = {}
	with open(RT_TUP_FILE) as fin:
		counter = 0
		for line in fin:
			line = line[:-1]
			author, retweeted_user, msg_id, ts = line.split('\t')
			timestamp = datetime.strptime(ts, "%a %b %d %H:%M:%S +0000 %Y")
			date = to_standard_date(timestamp)
			if not date in date_dict:
				date_dict[date] = []
			date_dict[date].append((author, retweeted_user, msg_id, timestamp))
			counter += 1
			if counter % 100000 == 0:
				print("{0} records processed".format(counter))
				
	
	with open(OUT_DICT_FILE, 'wb') as fout:
		pkl.dump(date_dict, fout)


if __name__ == '__main__':
	exit(main(sys.argv))