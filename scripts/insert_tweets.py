import sys
from models import RetweetModel
from sqlalchemy import create_engine
from sqlalchemy import or_, and_
from sqlalchemy import desc
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
from datetime import datetime



def to_mysql_datetime(ts):
	timestamp = datetime.strptime(ts,"%a %b %d %H:%M:%S +0000 %Y")
	return timestamp.strftime('%Y-%m-%d %H:%M:%S')




def establish_connection(url_params):
	db_url = URL(**url_params)
	engine = create_engine(db_url, pool_recycle=300)
	conn = engine.connect()
	
	# create session
	Session = sessionmaker()
	Session.configure(bind=engine)
	session = Session()
	return conn, session


def main(args):
	FILE_PATH = args[1]
	url_params = {  'drivername': 'mysql+mysqldb',
					'username': 'root',
					'password': 'letmein',
					'host': 'localhost',
					'database': 'pswitch'}

	conn, session = establish_connection(url_params)


	with open(FILE_PATH, 'r') as fin:
		counter = 1
		for line in fin:
			line = line[:-1]
			author, retweeted_user, msg_id, datetime = line.split('\t')
			datetime = to_mysql_datetime(datetime)
			msg_id = int(msg_id.split("_")[-1])
			retweet = RetweetModel(user_id=author.lower(), retweeted_user=retweeted_user.lower(),
									msg_id=msg_id, timestamp=datetime)
			session.add(retweet)
			if counter % 1000000 == 0:
				print("Inserting {0} retweets".format(counter))
				session.commit()
				print("Insertion complete for {0} retweets".format(counter))
			counter += 1
		session.commit()
	session.close()
	return 0


if __name__ == '__main__':
	exit(main(sys.argv))