import pickle as pkl
import sys
from neo4j.v1 import GraphDatabase



def main(args):
	DICT_FILE = args[1]

	driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
	with driver.session() as session:
		query = "MATCH (n:User) RETURN n.name"
		results = session.run(query)
		users = [r['n.name'] for r in results]
		users = sorted(users)

		user_dict = {}
		for idx, u in enumerate(users):
			user_dict[u] = idx

		with open(DICT_FILE, 'wb') as fout:
			pkl.dump(user_dict, fout)


if __name__ == '__main__':
	exit(main(sys.argv))