import json
import sys
import os
from datetime import datetime, timedelta
import numpy as np
import networkx as nx
import random
from scipy.spatial.distance import cosine




def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


def load_json_obj(filepath):
	with open(filepath, 'r') as fin:
		return json.loads(fin.read())


def load_list(filepath):
	with open(filepath) as fin:
		return list(filter(len, fin.read().split('\n')))


def write_list(filepath, l):
	with open(filepath, 'w') as fout:
		fout.write('\n'.join(l))



def clip_list(seeds, degree, threshold):
	if len(seeds) <= threshold:
		return seeds
	idx = np.argsort(np.array(degree))
	return np.array(seeds)[idx][-threshold:].tolist()


def pick_node(neighbors):
	keys = []
	vals = []
	for key in neighbors:
		keys.append(key)
		vals.append(neighbors[key]['weight'])

	keys = np.array(keys)
	vals = np.array(vals)

	probs = vals / np.sum(vals)
	return np.random.choice(keys, p=probs)


def get_polarity(graph, seeds, restart_prob, num_users):
	walkers = seeds[:]

	NUM_ITER = 1000
	# history = -1 * len(walkers)

	ucount = np.zeros(num_users)

	old_uprob_dist = None
	uprob_dist = None

	THRESHOLD = 1e-8
	cdist = None

	while (cdist is None) or (cdist > THRESHOLD):
		old_uprob_dist = uprob_dist

		for counter in range(NUM_ITER):
			for i in range(len(walkers)):
				ucount[walkers[i]] += 1
				if random.random() < restart_prob:
					walkers[i] = seeds[i]
				else:
					neighbors = graph[walkers[i]]
					
					# if history[i] in neighbors:
					# 	del neighbors[history[i]]
					# history[i] = walkers[i]

					walkers[i] = pick_node(neighbors)
		
		uprob_dist = ucount / np.sum(ucount)		
		if not old_uprob_dist is None:
			cdist = cosine(old_uprob_dist, uprob_dist)
			print("Cosine distance: {0}".format(cdist))
	print("Converged, cosine distance: {0}".format(cdist))
	return uprob_dist


def compute_polarity(tups):
	pols = []
	for tup in tups:
		dem, rep = tup
		diff = dem - rep
		pols.append(diff)
	return pols


def main(args):
	RES_DIR = args[1]
	
	filenames = os.listdir(RES_DIR)
	dates = sorted(list(set(['-'.join(d.split('.')[0].split('-')[:-1]) for d in filenames])))
	
	for date in dates:
		print("Processing {0}...".format(date))
		ulist_filepath = os.path.join(RES_DIR, "{0}-ulist.txt".format(date))
		elist_filepath = os.path.join(RES_DIR, "{0}-elist.txt".format(date))
		dem_seed_filepath = os.path.join(RES_DIR, "{0}-dem_seeds.txt".format(date))
		rep_seed_filepath = os.path.join(RES_DIR, "{0}-rep_seeds.txt".format(date))
		pred_pol_filepath = os.path.join(RES_DIR, "{0}-upol_pred.txt".format(date))

		ulist = load_list(ulist_filepath)
		elist = load_list(elist_filepath)
		dem_seeds = sorted([int(d) for d in load_list(dem_seed_filepath)])
		rep_seeds = sorted([int(r) for r in load_list(rep_seed_filepath)])

		graph = nx.Graph()
		graph.add_nodes_from(list(range(len(ulist))))

		edges = []
		for edge in elist:
			n1, n2, weight = edge.split('\t')
			n1 = int(n1)
			n2 = int(n2)
			weight = float(weight)
			edges.append((n1, n2, weight))

		graph.add_weighted_edges_from(edges)


		dem_degrees = [t[1] for t in graph.degree(dem_seeds)]
		rep_degrees = [t[1] for t in graph.degree(rep_seeds)]

		dem_seeds = list(filter(lambda x: graph.degree(x) > 0, dem_seeds))
		rep_seeds = list(filter(lambda x: graph.degree(x) > 0, rep_seeds))

		if len(dem_seeds) > 10:
			dem_seeds = random.sample(dem_seeds, 10)

		if len(rep_seeds) > 10:
			rep_seeds = random.sample(rep_seeds, 10)


		RESTART_PROB = 0.85
		if len(dem_seeds) > 0 and len(rep_seeds) > 0:
			print("Starting Random Walk for Democrats")
			dem_pol = get_polarity(graph, dem_seeds, RESTART_PROB, len(ulist))

			print("Starting Random Walk for Republicans")
			rep_pol = get_polarity(graph, rep_seeds, RESTART_PROB, len(ulist))


			tups = []
			for i in range(dem_pol.shape[0]):
				tups.append((dem_pol[i], rep_pol[i]))

			pol = compute_polarity(tups)
			write_list(pred_pol_filepath, [str(p) for p in pol])



if __name__ == '__main__':
	exit(main(sys.argv))