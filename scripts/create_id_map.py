import sys
import json
import progressbar
import pickle as pkl


def main(args):
	DATA_FILE = args[1]
	OUT_FILE = args[2]

	bar = progressbar.ProgressBar(max_value=24613385)

	tmap_dict = {}
	with open(DATA_FILE) as fin:
		for line in bar(fin):
			d = json.loads(line)
			if 'retweeted_msg_id' in d:
				rt_id = d['retweeted_msg_id']
				msg_id = d['msg_id']
				rt_id = rt_id.split("_")[-1]
				tmap_dict[rt_id] = str(msg_id)

	with open(OUT_FILE, 'wb') as fout:
		pkl.dump(tmap_dict, fout)

if __name__ == '__main__':
	exit(main(sys.argv))