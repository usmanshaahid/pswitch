from neo4j.v1 import GraphDatabase
import numpy as np


file = '4trump.txt'
out_file = '4trump_new.txt'

with open(file, 'r') as fin, open(out_file, 'w') as fout:
	users = fin.read().split('\n')
	counts = []
	for u in users:
		driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
		with driver.session() as session:
			query = "MATCH (u:User)-[r]-(t:Tweet) WHERE u.name = {name} RETURN COUNT(t) as count"
			result = session.run(query, name=u)
			count = result.single()['count']
			counts.append(count)
	users = np.array(users)
	counts = np.array(counts)

	TOP_K = 100
	sorted_idx = np.argsort(counts)[::-1][:TOP_K]
	users = users[sorted_idx]
	counts = counts[sorted_idx]

	print(users)
	print(counts)

	fout.write('\n'.join(users))